﻿using System;
using System.Linq;

namespace PrintRectangle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("To draw a rectangle enter the height and length with a space between");
            Console.WriteLine("To exit the program type \"e\"");
            while (true)
            {
                string size = Console.ReadLine();
                if(size == "e")
                { 
                    break;
                }

                string[] numbers = size.Split(" ");

                //Convert the height and length read to an int
                if(numbers.Length<2)
                {
                    Console.WriteLine("You must input two numbers with a space between them");
                }
                else
                {
                    bool isParsableHeight = Int32.TryParse(numbers[0], out int heightNumber);
                    bool isParsableLength = Int32.TryParse(numbers[1], out int lengthNumber);
                    if (!isParsableHeight && !isParsableLength)
                    {
                        Console.WriteLine("You must input two numbers with a space between them");
                    }
                    else
                    {
                        PrintRectangle(lengthNumber, heightNumber);
                        Console.WriteLine("Write a new size to draw a new rectangle");
                        Console.WriteLine("write e to exit");
                    }
                }

            }
        }

        static void PrintRectangle(int length, int height)
        {
            //iterate the height
            for (int i = 1; i <= height; i++)
            {
                string lineToPrint = "";
                //iterate the length
                for (int j = 1; j <= length; j++)
                {
                    //print a character when it is the first row or column
                    if( i == 1 || i == height || j == 1 || j == length)
                    {
                        lineToPrint += "O";
                    }
                    //the center of the rectangle is empty
                    else
                    {
                        lineToPrint += " ";
                    }
                }
                Console.WriteLine(lineToPrint);
            }
        }
    }
}
